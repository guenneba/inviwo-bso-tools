/*********************************************************************************
 *
 * Inviwo - Interactive Visualization Workshop
 *
 * Copyright (c) 2021 Inviwo Foundation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *********************************************************************************/

#include <ubordeaux/stomatools/processors/segmentation.h>

#include <inviwo/core/util/volumeramutils.h>
#include <inviwo/core/util/indexmapper.h>
#include <inviwo/core/util/templatesampler.h>
#include <inviwo/core/datastructures/volume/volume.h>
#include <inviwo/core/datastructures/volume/volumeram.h>
#include <inviwo/core/datastructures/volume/volumeramprecision.h>

#include <Eigen/Dense>
#include <queue>
#include <string.h>
#include <iostream>
#include <fstream>

typedef SparseMatrix<float> SpMat;
typedef PermutationMatrix<Dynamic> Permutation;
typedef Eigen::Triplet<float> T;

#define NB_NEIGHBOURS 6

namespace inviwo {

// The Class Identifier has to be globally unique. Use a reverse DNS naming scheme
const ProcessorInfo Segmentation::processorInfo_{
    "fr.ubordeaux.Segmentation",     // Class identifier
    "Segmentation",                // Display name
    "Stoma Tools",                         // Category
    CodeState::Experimental,       // Code state
    Tags::CPU,                     // Tags
};
const ProcessorInfo Segmentation::getProcessorInfo() const { return processorInfo_; }

Segmentation::Segmentation()
    : Processor()
    , volumeInport_("volumeInport")
    , maskInport_ ("maskInport")
    , probabilitiesOutport("probabilitiesOutport")
    , colorOutport("colorOutport")
    , printMeasurements_(
          "printDensity", "Print Density", [this](Event* e) { printMeasurements(e); }, IvwKey::P,
          KeyState::Release)
    , applySegmentation_("applySegmentation", "Apply Segmentation", false, InvalidationLevel::Valid)
    , beta_("beta", "Beta", 10, 0, 1000, 1)
    {

    
    addPort(volumeInport_);
    addPort(maskInport_);
    addPort(probabilitiesOutport);
    addPort(colorOutport);

    addProperty(beta_);
    addProperty(applySegmentation_);
    addProperty(printMeasurements_);
    
    applySegmentation_.onChange([&]() { process(); });
}

void Segmentation::createLaplacianMatrix(VectorXf volumeData, SpMat &L) {
    int n = volumeData.size();
    std::vector<T> tripletList;
    tripletList.reserve(n);
    util::IndexMapper3D index(volumeDimensions);

    for (size_t z = 0; z < volumeDimensions.z; z++) {
        for (size_t y = 0; y < volumeDimensions.y; y++) {
            for (size_t x = 0; x < volumeDimensions.x; x++) {
                auto index_i = index({x, y, z});
                auto gi = volumeData[index_i];
                float wii = 0.f;

                //:COMMENT:05/03/2021:MARTIN-DELOZANNE:Indexes of all the neighbours 
                // of the vertex of index index_i
                size_t index[NB_NEIGHBOURS] = { index_i - 1, index_i + 1, index_i - volumeDimensions.x, 
                                                index_i + volumeDimensions.x, index_i - (volumeDimensions.x * volumeDimensions.y), 
                                                index_i + (volumeDimensions.x * volumeDimensions.y)};

                //:COMMENT:05/03/2021:MARTIN-DELOZANNE:Conditions on neighbors' 
                // indexes not to go out of volume
                bool conditions[NB_NEIGHBOURS] = {x > 0, x < volumeDimensions.x - 1, 
                    y > 0, y < volumeDimensions.y - 1, z > 0, z < volumeDimensions.z - 1};

                for(int i = 0; i < NB_NEIGHBOURS; i++){
                    if(conditions[i]){
                        int index_j = index[i];

                        auto gj = volumeData[index_j];
                        auto gi_gj = gi - gj;

                        //:COMMENT:05/03/2021:MARTIN-DELOZANNE:Formula of the weight of the edge (index_i, index_j)
                        float wij = exp(-beta_ * (gi_gj * gi_gj));
                        tripletList.push_back(T(index_i, index_j, -wij));
                        wii += wij;
                    }
                }
                tripletList.push_back(T(index_i, index_i, wii));
            }
        }
    }
    L.setFromTriplets(tripletList.begin(), tripletList.end());
}

int Segmentation::createPermutation(VectorXf volumeData, VectorXf proba, Permutation &perm){
    int n = volumeData.size();
    perm.resize(n);

    int nb_unknowns = 0;
    int nb_knowns = n - 1;

    //:COMMENT:02/03/2021:MARTIN-DELOZANNE:Only the unknown probabilities (equal to -1) 
    // need to be updated. We reorder the equations and the probability to isolate the unknown values
    for(int i = 0; i < n; i++){
        if(proba[i] == -1){
            perm.indices()[i] = nb_unknowns;
            nb_unknowns++;
        }
        else{
            perm.indices()[i] = nb_knowns;
            nb_knowns--;
        }
    }
    return nb_unknowns;
}

void Segmentation::solveLinearEquation(VectorXf volumeData, VectorXf& proba, bool dimModified){
    int n = volumeData.size();
    
    SpMat L(n, n);
    //:COMMENT:02/03/2021:MARTIN-DELOZANNE:Recreate the Laplacian matrix only if the volume source has changed
    if(dimModified){
        //:COMMENT:02/03/2021:MARTIN-DELOZANNE:20210302: 1 - Create the sparse Laplacian matrix
        createLaplacianMatrix(volumeData, L);
        Laplacian_ = L;
    }

    else
        L = Laplacian_;

    //:COMMENT:02/03/2021:MARTIN-DELOZANNE: 2 - Create the permutation matrix putting
    // the fixed values at the end, and the true unknown at the beginning
    Permutation perm;
    
    int nbUnknows = createPermutation(volumeData, proba, perm);

    //:COMMENT:02/03/2021:MARTIN-DELOZANNE: 3 - Apply the permutation to both 
    // rows (equations) and columns (unknowns), i.e., L = P * L * P^-1
    L = L.twistedBy(perm);
    
    SpMat Lu = L.topLeftCorner(nbUnknows, nbUnknows);
    SpMat Lm = L.topRightCorner(nbUnknows, n - nbUnknows);
    
    //:COMMENT:02/03/2021:MARTIN-DELOZANNE: 4 - solve L * [x^T u^T]^T = 0, i.e., Lu * pu = - Lm * pm
    MatrixXf probaPrime = perm * proba;

    ConjugateGradient<SpMat, Lower|Upper> solver;
    solver.compute(Lu);
    probaPrime.topRows(nbUnknows) = solver.solve(-Lm * probaPrime.bottomRows(n - nbUnknows));

    //:COMMENT:02/03/2021:MARTIN-DELOZANNE: 5 - Copy back the results to proba
    probaPrime = perm.inverse() * probaPrime;
    proba = probaPrime;
}

VectorXf Segmentation::getVolumeData(const Volume& volume, bool mask){
    VectorXf v(volumeDimensions.x * volumeDimensions.y * volumeDimensions.z);
    int index = 0;

    for (size_t z = 0; z < volumeDimensions.z; z++) {
        for (size_t y = 0; y < volumeDimensions.y; y++) {
            for (size_t x = 0; x < volumeDimensions.x; x++) {
                if(mask){
                    auto pixelValue = maskInport_.getData()->getRepresentation<VolumeRAM>()->getAsDouble
                                                                                            ({x, y, z});

                    if(pixelValue == (double) MaskState::KEEP_THRESHOLD || 
                        pixelValue == (double) MaskState::KEEP_PAINTING){
                        v[index] = 1;
                    }
                    else if(pixelValue == (double) MaskState::REJECT_THRESHOLD || 
                        pixelValue == (double) MaskState::REJECT_PAINTING){
                        v[index] = 0;
                    }
                    else{
                        v[index] = -1;
                    }
                }

                else{
                    auto pixelValue = volumeInport_.getData()->getRepresentation<VolumeRAM>()
                                                        ->getAsNormalizedDouble({x, y, z});
                    v[index] = pixelValue;
                }
                index++;
            }
        }
    }
    return v;
}

std::shared_ptr<Volume> Segmentation::applyProbabilitiesToVolumeData(const Volume& volume, VectorXf proba){
    auto newVolumeRep = std::make_shared<VolumeRAMPrecision<unsigned short>>(volumeDimensions);
    auto newVolume = std::make_unique<Volume>(newVolumeRep);
    newVolume->setModelMatrix(volume.getModelMatrix());
    newVolume->setWorldMatrix(volume.getWorldMatrix());
    
    newVolume->getRepresentation<VolumeRAM>()->dispatch<void, dispatching::filter::Integers>([&](auto vol) {
        
        using ValueType = unsigned short;

        util::IndexMapper3D index(volumeDimensions);

        util::forEachVoxel(*vol, [&](const size3_t& pos) {
            newVolume->getEditableRepresentation<VolumeRAM>()->setFromNormalizedDouble(pos, proba[index(pos)]);
        });
    });
    return newVolume;
}

std::shared_ptr<Volume> Segmentation::applyProbabilitiesToVolumeColor(VectorXf proba){
    auto newVolume = std::make_shared<Volume>(volumeDimensions, DataVec4UInt8::get());

    util::IndexMapper3D index(volumeDimensions);

    for (size_t z = 0; z < volumeDimensions.z; z++) {
        for (size_t y = 0; y < volumeDimensions.y; y++) {
            for (size_t x = 0; x < volumeDimensions.x; x++) {
                if(proba[index({x, y, z})] > 0.5){
                    newVolume->getEditableRepresentation<VolumeRAM>()->setFromDVec4(
                                                {x, y, z}, dvec4(255, 64, 64, 255));
                }else
                    newVolume->getEditableRepresentation<VolumeRAM>()->setFromDVec4(
                                                {x, y, z}, dvec4(64, 64, 255, 255));
            }
        }
    }
    return newVolume;
}

void Segmentation::separateInteriorFromExterior(VectorXf proba, size3_t seed, std::vector<bool> &mark){
    //:COMMENT:17/03/2021:CHAUVEAU:A FIFO is needed to implement the Breadth First Search (BFS)
    std::queue<size3_t> myqueue;

    util::IndexMapper3D index(volumeDimensions);
    unsigned long cpt = 0;

    myqueue.push(seed);
    mark[index(seed)] = true;

    while(! myqueue.empty()){
        auto voxel = myqueue.front();
        myqueue.pop();

        size3_t neighbour[NB_NEIGHBOURS] = {{voxel.x - 1, voxel.y, voxel.z}, {voxel.x + 1, voxel.y, voxel.z}, 
            {voxel.x, voxel.y - 1, voxel.z}, {voxel.x, voxel.y + 1, voxel.z}, 
            {voxel.x, voxel.y, voxel.z - 1}, {voxel.x, voxel.y, voxel.z + 1}};

        bool conditions[NB_NEIGHBOURS] = {voxel.x > 0, voxel.x < volumeDimensions.x - 1, 
            voxel.y > 0, voxel.y < volumeDimensions.y - 1, 
            voxel.z > 0, voxel.z < volumeDimensions.z - 1};

        for(int i = 0; i < NB_NEIGHBOURS; i++){
            if(conditions[i]){
                size3_t indice = neighbour[i];
                //:COMMENT:17/03/2021:CHAUVEAU:The voxel has not already been
                // visited and is part of the outside air
                if(! mark[index(indice)] && proba[index(indice)] < 0.5){
                    myqueue.push(indice);
                    mark[index(indice)] = true;
                }
            }
        }
        cpt++;
    }
    numberOfVoxels[(int)(SegmentedParts::OUTSIDE_AIR)] = cpt;
}

void Segmentation::separateMatterFromAir(VectorXf proba, std::vector<bool> &mark){
    int matter = 0, interior_air = 0;

    for(size_t i = 0; i < mark.size(); i++){
        //:COMMENT:17/03/2021:CHAUVEAU:Voxels that have not already been visited
        // are part of the interior of the plant
        if(! mark[i]){
            if(proba[i] >= 0.5)
                matter++;
            else
                interior_air++;
        }
    }
    numberOfVoxels[(int)(SegmentedParts::MATTER)] = matter;
    numberOfVoxels[(int)(SegmentedParts::INTERIOR_AIR)] = interior_air;
}

void Segmentation::printMeasurements(Event* theevent) {
    auto fileName = volumeInport_.getData()->getMetaData<StringMetaData>("filename")->get();

    int outside_air = numberOfVoxels[(int)(SegmentedParts::OUTSIDE_AIR)];
    int matter = numberOfVoxels[(int)(SegmentedParts::MATTER)];
    int interior_air = numberOfVoxels[(int)(SegmentedParts::INTERIOR_AIR)];
    int proportionAirInsideLeaf = (matter + interior_air) > 0 ? (interior_air / (double)(matter + interior_air)) * 100 : 0;
    int proportionMatterInsideLeaf = (matter + interior_air) > 0 ? (matter / (double)(matter + interior_air)) * 100 : 0;

    fileName.erase(fileName.rfind('.'));
    fileName += "_info.txt";
    std::ofstream ofs(fileName.c_str());
    
    ofs <<  "Dimensions : " << volumeDimensions << std::endl;
    ofs <<  "Total volume : " << outside_air + matter + interior_air << " voxels" << std::endl;
    ofs <<  "Outside air volume : " << outside_air << " voxels" << std::endl;
    ofs <<  "Volume of the matter : " << matter << " voxels" << std::endl;
    ofs <<  "Interior air volume : " << interior_air << " voxels" << std::endl;
    ofs <<  "Total volume of the leaf : " << matter + interior_air << " voxels" << std::endl;
    ofs <<  "Proportion of air inside the leaf : " << proportionAirInsideLeaf << "%" << std::endl;
    ofs << "Proportion of matter inside the leaf : " << proportionMatterInsideLeaf << "%" << std::endl;
    
    ofs.close();
}

void Segmentation::process() {
    auto volume = volumeInport_.getData();

    if(applySegmentation_){
        auto mask = maskInport_.getData();
        auto dimModified = volumeDimensions != volume->getDimensions();
        volumeDimensions = volume->getDimensions();

        auto volumeData = getVolumeData(*volume, false);
        auto proba = getVolumeData(*mask, true);

        solveLinearEquation(volumeData, proba, dimModified);

        probabilityVolume = applyProbabilitiesToVolumeData(*volume, proba);
        colorVolume = applyProbabilitiesToVolumeColor(proba);

        std::vector<bool> mark(proba.size() ,false);
        //:COMMENT:17/03/2021:CHAUVEAU:One of the voxels of the "highest" slice is probably outside air
        size3_t seed = {volumeDimensions.x - 1, volumeDimensions.y -1, volumeDimensions.z - 1};
        separateInteriorFromExterior(proba, seed, mark);
        separateMatterFromAir(proba, mark);
    }

    if(probabilityVolume == NULL){
        probabilitiesOutport.setData(volume);
        colorOutport.setData(volume);
    }

    else{
        probabilitiesOutport.setData(probabilityVolume);
        colorOutport.setData(colorVolume);
    }
}
}  // namespace inviwo