/*********************************************************************************
 *
 * Inviwo - Interactive Visualization Workshop
 *
 * Copyright (c) 2021 Inviwo Foundation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *********************************************************************************/

#include <ubordeaux/stomatools/processors/framepainter.h>
#include <inviwo/core/interaction/events/mouseevent.h>
#include <inviwo/core/interaction/events/keyboardevent.h>
#include <inviwo/core/datastructures/image/layerram.h>
#include <inviwo/core/processors/canvasprocessor.h>

namespace inviwo {

// The Class Identifier has to be globally unique. Use a reverse DNS naming scheme
const ProcessorInfo FramePainter::processorInfo_{
    "fr.ubordeaux.FramePainter",  // Class identifier
    "Frame Painter",            // Display name
    "Stoma Tools",                      // Category
    CodeState::Experimental,    // Code state
    Tags::CPU,                  // Tags
};
const ProcessorInfo FramePainter::getProcessorInfo() const { return processorInfo_; }

FramePainter::FramePainter()
    : Processor()
    , inportX_("inportX_", true)
    , inportY_("inportY_", true)
    , inportZ_("inportZ_", true)
    , volumeInport_("volumeInport")
    , maskOutportX_("maskOutportX_", false)
    , maskOutportY_("maskOutportY_", false)
    , maskOutportZ_("maskOutportZ_", false)
    , mouseEventsProperties_("mouseEventProperties_", "MouseEvents")
    , maskOutport3D_("maskOutPort3D_")
    , mousePaint_(
          "mousePaint", "Mouse Paint", [this](Event* e) { mousePaintEvent(e); }, MouseButton::Left,
          MouseState::Move | MouseState::Press)
    , mousePaintUnwant_(
          "mousePaintUnwantEvent", "Mouse Paint Unwant",
          [this](Event* e) { mousePaintUnwantEvent(e); }, MouseButton::Left,
          MouseState::Move | MouseState::Press, KeyModifier::Shift)
    , mouseErase_(
          "mouseErase", "Mouse Erase", [this](Event* e) { mouseEraseEvent(e); }, MouseButton::Left,
          MouseState::Move | MouseState::Press, KeyModifier::Control)
    , resetMask_(
          "resetMask", "Reset Mask", [this](Event* e) { clearMaskEvent(e); }, IvwKey::R,
          KeyState::Release)
    , undoPaint_(
          "undoPaint", "Undo Painting", [this](Event* e) { undoPainting(e); }, IvwKey::Z,
          KeyState::Press, KeyModifier::Alt)
    , thresholdingProperties_("thresholdingProperties_", "Thresholding")
    , max_("maxThresold", "Max threshold", 0.9f, 0.f, 1.f, 0.01f)
    , min_("minThresold", "Min threshold", 0.1f, 0.f, 1.f, 0.01f)
    , slicesProperties_("slicesProperties_", "Slices Control")
    , brushProperties_("brushProperties_", "Brush Settings")
    , brush_("brushSize", "Brush Size", 1, 1, 10) {

    sliceNumbers_.push_back(IntSizeTProperty("sliceNumberX", "Slice Number X", 1, 1));
    sliceNumbers_.push_back(IntSizeTProperty("sliceNumberY", "Slice Number Y", 1, 1));
    sliceNumbers_.push_back(IntSizeTProperty("sliceNumberZ", "Slice Number Z", 1, 1));

    addPort(inportX_);
    addPort(inportY_);
    addPort(inportZ_);
    addPort(volumeInport_);

    addPort(maskOutportX_);
    addPort(maskOutportY_);
    addPort(maskOutportZ_);
    addPort(maskOutport3D_);

    addProperties(mouseEventsProperties_, thresholdingProperties_, slicesProperties_,
                  brushProperties_);
    mouseEventsProperties_.addProperties(mousePaint_, mousePaintUnwant_, mouseErase_, resetMask_,
                                         undoPaint_);
    thresholdingProperties_.addProperties(max_, min_);
    slicesProperties_.addProperties(sliceNumbers_[0], sliceNumbers_[1], sliceNumbers_[2]);
    brushProperties_.addProperty(brush_);

    thresholdingProperties_.onChange([this]() { onThresholdChange(); });
}

FramePainter::~FramePainter() {}
void FramePainter::process() {
    checkAndResetMasks();

    get2DMaskValue();

    maskOutportX_.setData(masks_[0]);
    maskOutportY_.setData(masks_[1]);
    maskOutportZ_.setData(masks_[2]);

    maskOutport3D_.setData(mask3D_);
}

void FramePainter::onThresholdChange() {
    size3_t size = mask3D_->getDimensions();
    for (unsigned int i = 0; i < size[0]; ++i) {
        for (unsigned int j = 0; j < size[1]; ++j) {
            for (unsigned int k = 0; k < size[2]; ++k) {
                auto color = MaskState::UNKNOWN;
                auto maskColor = mask3D_->getRepresentation<VolumeRAM>()->getAsDouble({i, j, k});
                auto pixelValue =
                    volumeInport_.getData()->getRepresentation<VolumeRAM>()->getAsNormalizedDouble(
                        {i, j, k});
                if (pixelValue < min_)
                    color = MaskState::REJECT_THRESHOLD;
                else if (pixelValue > max_)
                    color = MaskState::KEEP_THRESHOLD;
                else if (maskColor == (double)MaskState::KEEP_PAINTING)
                    color = MaskState::KEEP_PAINTING;
                else if (maskColor == (double)MaskState::REJECT_PAINTING)
                    color = MaskState::REJECT_PAINTING;
                mask3D_->getEditableRepresentation<VolumeRAM>()->setFromDouble({i, j, k},
                                                                               (double)color);
            }
        }
    }
    get2DMaskValue();
}

void FramePainter::mousePaintEvent(Event* theevent) {
    if (!inportX_.hasData() || !inportY_.hasData() || !inportZ_.hasData()) {
        return;
    }
    paintState_ = PaintState::KEEP;
    paint(theevent);
}

void FramePainter::mousePaintUnwantEvent(Event* theevent) {
    if (!inportX_.hasData() || !inportY_.hasData() || !inportZ_.hasData()) {
        return;
    }
    paintState_ = PaintState::REJECT;
    paint(theevent);
}

void FramePainter::mouseEraseEvent(Event* theevent) {
    if (!inportX_.hasData() || !inportY_.hasData() || !inportZ_.hasData()) {
        return;
    }
    paintState_ = PaintState::ERASE;
    paint(theevent);
}

void FramePainter::clearMaskEvent(Event* theevent) {
    clearMasks();
    min_ = 0;
    max_ = 1;
    invalidate(InvalidationLevel::InvalidOutput);
}

void FramePainter::undoPainting(Event* theevent) {
    if (!memory_.empty()) {
        std::tuple<std::shared_ptr<Volume>, std::shared_ptr<Image>, std::shared_ptr<Image>,
                   std::shared_ptr<Image>>
            modification = memory_.back();
        memory_.pop_back();

        mask3D_ = std::get<0>(modification);

        masks_[0] = std::get<1>(modification);
        masks_[1] = std::get<2>(modification);
        masks_[2] = std::get<3>(modification);

        invalidate(InvalidationLevel::InvalidOutput);
    }
}

void FramePainter::paint(Event* theevent) {
    if (auto mouseEvent = theevent->getAs<MouseEvent>()) {

        saveState();

        int workingMask = getWorkingMask(mouseEvent->getVisitedProcessors());

        if (workingMask == -1) {
            return;
        }

        dvec2 pos2D = getPos2D(workingMask, mouseEvent->pos(), mouseEvent->canvasSize());
        if (pos2D == dvec2(-1, -1)) {
            return;
        }

        int radius = brush_.get() - 1;
        if (radius > 0) {
            applyBrush(workingMask,pos2D,radius);
        } else {
            dvec3 pos3D = getPos3D(workingMask,pos2D);
            update2DMask(workingMask, pos3D);
            update3DMask(pos3D);
        }

        invalidate(InvalidationLevel::InvalidOutput);
    }
}
void FramePainter::applyBrush(int mask, dvec2 pos,int radius){
    int minRow = 0;
    int minCol = 0;
    unsigned int maxRow = 0;
    unsigned int maxCol = 0;
    size2_t dim = masks_[mask]->getDimensions();
    
    minCol = std::max(0.0, pos.x - radius);
    maxCol = std::min(pos.x + radius, (double)dim.x);
    minRow = std::max(0.0, pos.y - radius);
    maxRow = std::min(pos.y + radius, (double)dim.y);

    dvec3 pos3D;

    for (unsigned int i = minCol; i < maxCol + 1; ++i) {
        for (unsigned int j = minRow; j < maxRow + 1; ++j) {
            if (sqrt((i - pos.x) * (i - pos.x) + (j - pos.y) * (j - pos.y)) <=
                radius) {
                pos3D = getPos3D(mask,dvec2(i,j));
            } else {
                continue;
            }
            update2DMask(mask, pos3D);
            update3DMask(pos3D);
        }
    }
}
int FramePainter::getWorkingMask(std::vector<Processor*> processorList) {
    //:TRICKY:04/03/2021:MONTFERME:In order to check on which axis to paint one can use the
    // list of visited processor during the event propagation then check to which outport are
    // the visited processor connected to has a processor input can only be connected to only
    // one outport if (imgRatio < 1.0
    CanvasProcessor* canvas;
    if (processorList.size() > 0) {
        canvas = dynamic_cast<CanvasProcessor*>(processorList[0]);
        if (canvas) {
            int idx = 1;
            while (idx < (int)processorList.size()) {
                Processor* p = processorList[idx];
                std::vector<Inport*> inports = p->getInports();
                for (Inport* in : inports) {
                    if (in->isConnectedTo(&maskOutportX_)) {
                        return 0;
                    } else if (in->isConnectedTo(&maskOutportY_)) {
                        return 1;
                    } else if (in->isConnectedTo(&maskOutportZ_)) {
                        return 2;
                    }
                }
                idx++;
            }
        }
    }
    return -1;
}

dvec2 FramePainter::getPos2D(int mask, dvec2 mousePos, uvec2 canvasSize) {

    size2_t dim = masks_[mask]->getDimensions();
    double imgRatio = (double)dim.y / (double)dim.x;
    double canvasRatio = (double)canvasSize.y / (double)canvasSize.x;

    dvec2 origin = {0, 0};
    dvec2 imgInCanvas = {0, 0};
    bool vertical = true;

    if (canvasRatio > 1.0) {
        if (imgRatio >= 1.0) {
            if (canvasRatio > imgRatio) {
                vertical = false;
            } else {
                vertical = true;
            }
        } else {
            vertical = false;
        }
    } else if (canvasRatio < 1.0) {
        if (imgRatio >= 1.0) {
            vertical = true;
        } else {
            if (canvasRatio < imgRatio) {
                vertical = true;
            } else {
                vertical = false;
            }
        }
    } else {  //:COMMENT:17/03/2021:MONTFERME:If canvasRatio == 1.0
        if (imgRatio >= 1.0) {
            vertical = true;
        } else {
            vertical = false;
        }
    }

    if (vertical) {
        origin.y = 0;
        imgInCanvas.y = canvasSize.y;
        imgInCanvas.x = (dim.x * canvasSize.y) / dim.y;
        origin.x = (canvasSize.x - imgInCanvas.x) / 2;
    } else {
        origin.x = 0;
        imgInCanvas.x = canvasSize.x;
        imgInCanvas.y = (dim.y * canvasSize.x) / dim.x;
        origin.y = (canvasSize.y - imgInCanvas.y) / 2;
    }
    if (glm::any(glm::greaterThan(mousePos, origin + imgInCanvas)) ||
        glm::any(glm::lessThan(mousePos, origin))) {
        return {-1, -1};
    } else {
        mousePos.x = (mousePos.x - origin.x) / (imgInCanvas.x - 1);
        mousePos.y = (mousePos.y - origin.y) / (imgInCanvas.y - 1);
    }
    dvec2 res = {mousePos * dvec2(dim - size2_t(1))};
    res.x = floor(res.x);
    res.y = floor(res.y);
    return res;
}

dvec3 FramePainter::getPos3D(int mask, dvec2 pos2D){
    const dvec3 currentSlices = getCurrentSlices();
    dvec3 pos3D;
    if (mask == 0) {
        pos3D = {currentSlices.x, pos2D.y, pos2D.x};
    } else if (mask == 1) {
        pos3D = {pos2D.x, currentSlices.y, pos2D.y};
    } else if (mask == 2) {
        pos3D = {pos2D.x, pos2D.y, currentSlices.z};
    }
    return pos3D;
}

void FramePainter::update2DMask(int mask, size3_t pos) {
    dvec4 color;

    switch (paintState_) {
        case PaintState::ERASE:
            color = dvec4(0, 0, 0, 0);
            break;
        case PaintState::REJECT:
            color = dvec4(0, 0, 255, 255);
            break;
        case PaintState::KEEP:
            color = dvec4(255, 0, 0, 255);
            break;
        default:
            break;
    }

    const dvec3 currentSlices = getCurrentSlices();
    const size3_t dims = mask3D_->getDimensions();
    dvec2 posAlongX(-1, -1);
    dvec2 posAlongY(-1, -1);
    dvec2 posAlongZ(-1, -1);
    if (mask == 0) {
        posAlongX = {pos.z, pos.y};
        if (pos.y == currentSlices.y) {
            posAlongY = {pos.x, pos.z};
        }
        if (pos.z == currentSlices.z) {
            posAlongZ = {pos.x, pos.y};
        }
    } else if (mask == 1) {
        posAlongY = {pos.x, pos.z};
        if (pos.x == currentSlices.x) {
            posAlongX = {pos.z, pos.y};
        }
        if (pos.z == currentSlices.z) {
            posAlongZ = {pos.x, pos.y};
        }
    } else if (mask == 2) {
        posAlongZ = {pos.x, pos.y};
        if (pos.x == currentSlices.x) {
            posAlongX = {pos.z, pos.y};
        }
        if (pos.y == currentSlices.y) {
            posAlongY = {pos.x, pos.z};
        }
    }

    if (glm::all(glm::greaterThanEqual(posAlongX, dvec2(0))) &&
        glm::all(glm::lessThan(posAlongX, dvec2(dims.z, dims.y)))) {
        masks_[0]->getColorLayer(0)->getEditableRepresentation<LayerRAM>()->setFromDVec4(posAlongX,
                                                                                         color);
    }
    if (glm::all(glm::greaterThanEqual(posAlongY, dvec2(0))) &&
        glm::all(glm::lessThan(posAlongY, dvec2(dims.x, dims.z)))) {
        masks_[1]->getColorLayer(0)->getEditableRepresentation<LayerRAM>()->setFromDVec4(posAlongY,
                                                                                         color);
    }

    if (glm::all(glm::greaterThanEqual(posAlongZ, dvec2(0))) &&
        glm::all(glm::lessThan(posAlongZ, dvec2(dims.x, dims.y)))) {
        masks_[2]->getColorLayer(0)->getEditableRepresentation<LayerRAM>()->setFromDVec4(posAlongZ,
                                                                                         color);
    }
}

void FramePainter::update3DMask(size3_t pos) {
    auto color = MaskState::UNKNOWN;
    switch (paintState_) {
        case PaintState::ERASE:
            color = MaskState::UNKNOWN;
            break;
        case PaintState::REJECT:
            color = MaskState::REJECT_PAINTING;
            break;
        case PaintState::KEEP:
            color = MaskState::KEEP_PAINTING;
            break;
        default:
            break;
    }
    auto pixelValue =
        volumeInport_.getData()->getRepresentation<VolumeRAM>()->getAsNormalizedDouble(pos);
    if (pixelValue < min_)
        color = MaskState::REJECT_THRESHOLD;

    else if (pixelValue > max_)
        color = MaskState::KEEP_THRESHOLD;
    if (glm::all(glm::greaterThanEqual(dvec3(pos), dvec3(0))) &&
        glm::all(glm::lessThan(dvec3(pos), dvec3(mask3D_->getDimensions())))) {
        mask3D_->getEditableRepresentation<VolumeRAM>()->setFromDouble(pos, (double)color);
    }
}

void FramePainter::checkAndResetMasks() {
    const size2_t sizeX = inportX_.getData()->getDimensions();
    const size2_t sizeY = inportY_.getData()->getDimensions();
    const size2_t sizeZ = inportZ_.getData()->getDimensions();
    const size3_t volumeDimensions = {sizeY[0], sizeX[1], sizeX[0]};

    if (mask3D_ == nullptr || mask3D_->getDimensions() != volumeDimensions) {
        mask3D_ = std::make_shared<Volume>(volumeDimensions, DataVec4UInt16::get());
        init3DMask();
    }
    if (masks_[0] == nullptr || masks_[0]->getDimensions() != sizeX) {
        masks_[0] = std::make_shared<Image>(sizeX, inportX_.getData()->getDataFormat());
        init2DMask(0);
    }
    if (masks_[1] == nullptr || masks_[1]->getDimensions() != sizeY) {
        masks_[1] = std::make_shared<Image>(sizeY, inportY_.getData()->getDataFormat());
        init2DMask(1);
    }
    if (masks_[2] == nullptr || masks_[2]->getDimensions() != sizeZ) {
        masks_[2] = std::make_shared<Image>(sizeZ, inportZ_.getData()->getDataFormat());
        init2DMask(2);
    }
}

void FramePainter::clearMasks() {
    init3DMask();
    for (int i = 0; i < 3; ++i) {
        init2DMask(i);
    }
}

void FramePainter::init2DMask(int mask) {
    if (!(mask >= 0 && mask < 3)) {
        std::cerr << "invalid mask number" << std::endl;
        return;
    }

    size2_t size = masks_[mask]->getDimensions();
    for (unsigned int i = 0; i < size[0]; i++) {
        for (unsigned int j = 0; j < size[1]; j++) {
            masks_[mask]->getColorLayer(0)->getEditableRepresentation<LayerRAM>()->setFromDVec4(
                {i, j}, dvec4(0, 0, 0, 0));
        }
    }
}
void FramePainter::init3DMask() {
    size3_t size = mask3D_->getDimensions();
    for (unsigned int i = 0; i < size[0]; ++i) {
        for (unsigned int j = 0; j < size[1]; ++j) {
            for (unsigned int k = 0; k < size[2]; ++k) {
                auto color = MaskState::UNKNOWN;
                auto pixelValue =
                    volumeInport_.getData()->getRepresentation<VolumeRAM>()->getAsNormalizedDouble(
                        {i, j, k});
                if (pixelValue < min_)
                    color = MaskState::REJECT_THRESHOLD;
                else if (pixelValue > max_)
                    color = MaskState::KEEP_THRESHOLD;
                mask3D_->getEditableRepresentation<VolumeRAM>()->setFromDouble({i, j, k},
                                                                               (double)color);
            }
        }
    }
}

dvec4 FramePainter::getColorFromMask3D(double state) {
    if (state == (double)MaskState::KEEP_PAINTING)
        return dvec4(255, 0, 0, 255);
    else if (state == (double)MaskState::KEEP_THRESHOLD)
        return dvec4(247, 35, 12, 255);
    else if (state == (double)MaskState::REJECT_PAINTING)
        return dvec4(0, 0, 255, 255);
    else if (state == (double)MaskState::REJECT_THRESHOLD)
        return dvec4(44, 117, 255, 255);
    return dvec4(0, 0, 0, 0);
}

void FramePainter::get2DMaskValue() {

    size3_t size = mask3D_->getDimensions();
    const bool threshsoldModified = min_.isModified() || max_.isModified();
    const dvec3 currentSlices = getCurrentSlices();
    if (sliceNumbers_.at(0).isModified() || threshsoldModified) {
        int x = currentSlices.x;
        for (unsigned int y = 0; y < size[1]; ++y) {
            for (unsigned int z = 0; z < size[2]; ++z) {
                double maskState =
                    mask3D_->getEditableRepresentation<VolumeRAM>()->getAsDouble({x, y, z});
                dvec4 color = getColorFromMask3D(maskState);
                masks_[0]->getColorLayer(0)->getEditableRepresentation<LayerRAM>()->setFromDVec4(
                    {z, y}, color);
            }
        }
    }

    if (sliceNumbers_.at(1).isModified() || threshsoldModified) {
        int y = currentSlices.y;
        for (unsigned int x = 0; x < size[0]; ++x) {
            for (unsigned int z = 0; z < size[2]; ++z) {
                double maskState =
                    mask3D_->getEditableRepresentation<VolumeRAM>()->getAsDouble({x, y, z});
                dvec4 color = getColorFromMask3D(maskState);
                masks_[1]->getColorLayer(0)->getEditableRepresentation<LayerRAM>()->setFromDVec4(
                    {x, z}, color);
            }
        }
    }

    if (sliceNumbers_.at(2).isModified() || threshsoldModified) {
        int z = currentSlices.z;
        for (unsigned int x = 0; x < size[0]; ++x) {
            for (unsigned int y = 0; y < size[1]; ++y) {
                double maskState =
                    mask3D_->getEditableRepresentation<VolumeRAM>()->getAsDouble({x, y, z});
                dvec4 color = getColorFromMask3D(maskState);
                masks_[2]->getColorLayer(0)->getEditableRepresentation<LayerRAM>()->setFromDVec4(
                    {x, y}, color);
            }
        }
    }
}

const dvec3 FramePainter::getCurrentSlices(){
    return dvec3(sliceNumbers_[0].get() - 1, sliceNumbers_[1].get() - 1,
                              sliceNumbers_[2].get() - 1);
}

void FramePainter::saveState() {
    std::shared_ptr<Volume> mask3Dcopy = std::make_shared<Volume>(*mask3D_);

    std::shared_ptr<Image> xCopy = std::make_shared<Image>(*masks_[0]);
    std::shared_ptr<Image> yCopy = std::make_shared<Image>(*masks_[1]);
    std::shared_ptr<Image> zCopy = std::make_shared<Image>(*masks_[2]);

    std::tuple<std::shared_ptr<Volume>, std::shared_ptr<Image>, std::shared_ptr<Image>,
               std::shared_ptr<Image>>
        state = std::make_tuple(mask3Dcopy, xCopy, yCopy, zCopy);

    if (!(memory_.size() < memoryLimit_)) {
        memory_.erase(memory_.begin());
    }

    memory_.push_back(state);
}

}  // namespace inviwo