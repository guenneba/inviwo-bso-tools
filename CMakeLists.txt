#--------------------------------------------------------------------
# Inviwo stomatools Module
ivw_module(stomatools)

#--------------------------------------------------------------------
# Add header files
set(HEADER_FILES
include/ubordeaux/stomatools/stomatoolsmodule.h
include/ubordeaux/stomatools/stomatoolsmoduledefine.h
include/ubordeaux/stomatools/processors/framepainter.h
include/ubordeaux/stomatools/processors/segmentation.h
)
ivw_group("Header Files" ${HEADER_FILES})

#--------------------------------------------------------------------
# Add source files
set(SOURCE_FILES
src/stomatoolsmodule.cpp
    src/processors/framepainter.cpp
    src/processors/segmentation.cpp
   
)
ivw_group("Source Files" ${SOURCE_FILES})

#--------------------------------------------------------------------
# Add shaders
set(SHADER_FILES
)
ivw_group("Shader Files" ${SHADER_FILES})

#--------------------------------------------------------------------


#--------------------------------------------------------------------
# Create module
ivw_create_module(${SOURCE_FILES} ${HEADER_FILES} ${SHADER_FILES})

#--------------------------------------------------------------------
# Add shader directory to pack
# ivw_add_to_module_pack(${CMAKE_CURRENT_SOURCE_DIR}/glsl)
