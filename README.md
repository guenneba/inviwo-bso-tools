# Stomatools - a 3D analysis tool of the vascular system of plants in a context of severe drought

Repository for end-of-studies project of Master 2 in Computer Science for Image and Sound of Bordeaux 2020-2021.

## Table of Content

1. [Installation](#installation)
    1. [Dependencies](#dependencies)
    1. [On Linux](#on-linux)
        1. [From Source](#from-source)
    1. [On Windows](#on-windows) 
       1. [Using a Release](#using-a-release)
        2. [From Source](#from-source-1)
1. [Launching](#launching)
    1. [Linux](#linux)
    2. [Windows](#windows)
1. [Usage](#usage)
    1. [Frame Painter](#frame-painter)
    2. [Segmentation](#segmentation)
1. [Contributing](#contributing)
1. [Authors](#authors)
    


## Installation

### Dependencies

If you want to use stomatools on windows you can skip directly to [This part](#using-a-release)

You will need at least
- [CMake](https://cmake.org/download/) >= 3.12.0

- [Qt binaries](https://qt.io/download-open-source/) >= 5.12

Install and setup Inviwo by following the steps on the page : [Building Inviwo](https://inviwo.org/manual-gettingstarted-build.html).

**You will require to be able to build Inviwo if you want to build the stomatools module yourself.**


#### Note

Installing from source requires to recompile the Inviwo framework. For more details about this process, please check the official Inviwo build instruction page [here](https://inviwo.org/manual-gettingstarted-build.html)

### On Linux

#### From Source

1. Put the `stomatools` module in the directory `inviwo/modules`

2. Open CMake choose the `source code` path  and where to `build the binaries`.
Click `Configure` once to register all the necessary. Don't forget to check the box in the column `Value` corresponding to the module stomatools : `IVW_MODULE_STOMATOOLS`

3. Hit `Generate`

4. In the `build` folder compile the project with `make -j6` (Be warn that this step can take a long time depending on the system you build the program on).

### On Windows

#### Using a release

1. You can directly download and use stomatools from the release page [here](https://github.com/RobinMontferme/stomatools/releases/tag/v0.1).
It comes bundled with inviwo and it's required dependencies.

2. Unzip the provided archive to a location of you choice


#### From Source

1. Put the `stomatools`module in the directory `inviwo/modules`

2. Open CMake (cmake-gui is strongly recommended) choose the `source code` path  and where to `build the binaries`.
Click `Configure` once to register all the necessary. Don't forget to check the box in the column `Value` corresponding to the module stomatools : `IVW_MODULE_STOMATOOLS`

3. Hit `Generate`

4. Click on the `Open Project` button that allows the project to be open in Microsoft Visual Studio and click on the green arrow button to launch the compilation (Be warn that this step can take a long time depending on the system you build the program on).

Note that once the program has been compiled once, it is not necessary to open Visual Studio in order to launch it

## Launching

### Linux

In a terminal launch  the file `./Inviwo` in the `bin` folder inside the `build` directory.

### Windows

Double-click the file named `Inviwo.exe` in the `bin` folder inside the `build` directory. 
Or, launch the same file from command line using either `cmd` or `powershell` terminals.

If you are using a downloaded release, simply navigate to `bin` and launch `Inviwo.exe`


## Usage

To manipulate the tools contained in the stomatools module, open the workspace located in the folder `workspace`.

### Frame Painter

`Frame Painter` allows to annotate slices for segmentation. It offers different parameters in the property tab including two thresholds, the segment numbers and the brush radius. The `Max threshold` allows to annotate all values above the threshold as matter. The pixels annotated in this way will then be drawn in red on the corresponding 2D canvas. The `Min threshold` allows to annotate all values below the threshold as being air outside the plant. The pixels annotated in this way will then be drawn in blue on the corresponding 2D canvas. Moreover, to manually annotate a pixel it is possible to click directly in the 2D canvas. A simple click allows to annotate pixels corresponding to matter and will display the selected pixel(s) in red (of a slightly different hue). If the `SHIFT` key is used in addition to the click, the selected pixel(s) will be considered as air outside the plant and displayed in blue on the 2D canvas. 
The slice numbers are linked to the `Volume Slice Extracter` processors. It is therefore possible to navigate between slices from the `Frame Painter` processor. The brush radius allows you to draw on the slices with a larger brush. This one is limited to 10.

### Segmentation

The segmentation processor computes the segmentation using the random walker algorithm using the data annotated with the Frame Painter processor. It is possible to enable/disable segmentation in the properties, by clicking on the `Apply Segmentation` button. This can be useful to save resources while annotating data. This processor also offers the possibility to save different measurements corresponding to the number of pixels of each segmented part in a text file by pressing the `p` key.

## Contributing

You can freely fork the project to improve it or update it to whatever seems fit.

You can follow Inviwo's [contributing guidelines](https://inviwo.org/contributing.html)

See [License](#license).

Due to the nature of this project context, pull requests and issue are likely to be ignored.

## Authors

CHAUVEAU Amandine 

GUESDON Thomas

MARTIN-DELOZANNE Delphine

MONTFERME Robin

## License

Freely available under the Simplified BSD License.
