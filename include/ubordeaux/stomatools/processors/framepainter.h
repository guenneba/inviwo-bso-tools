/*********************************************************************************
 *
 * Inviwo - Interactive Visualization Workshop
 *
 * Copyright (c) 2021 Inviwo Foundation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *********************************************************************************/

#pragma once

#include <ubordeaux/stomatools/stomatoolsmoduledefine.h>
#include <inviwo/core/common/inviwo.h>
#include <inviwo/core/processors/processor.h>
#include <inviwo/core/ports/imageport.h>
#include <inviwo/core/ports/volumeport.h>
#include <inviwo/core/properties/ordinalproperty.h>
#include <inviwo/core/properties/eventproperty.h>
#include <inviwo/core/properties/buttonproperty.h>
#include <inviwo/core/properties/boolproperty.h>
#include <inviwo/core/properties/compositeproperty.h>
#include <inviwo/core/datastructures/geometry/geometrytype.h>
#include <iostream>

enum class PaintState {KEEP, REJECT, ERASE};
enum class MaskState {UNKNOWN = -1, REJECT_THRESHOLD, KEEP_THRESHOLD, REJECT_PAINTING, KEEP_PAINTING};

namespace inviwo {
// TODO :rmontferme:20210228:write the doc for this processor
/** \docpage{org.inviwo.helloworld, helloworld}
 * ![](org.inviwo.helloworld.png?classIdentifier=org.inviwo.helloworld)
 * Explanation of how to use the processor.
 *
 * ### Inports
 *   * __<Inport1>__ <description>.
 *
 * ### Outports
 *   * __<Outport1>__ <description>.
 *
 * ### Properties
 *   * __<Prop1>__ <description>.
 *   * __<Prop2>__ <description>
 */

/**
 * \brief VERY_BRIEFLY_DESCRIBE_THE_PROCESSOR
 * DESCRIBE_THE_PROCESSOR_FROM_A_DEVELOPER_PERSPECTIVE
 */
class IVW_MODULE_STOMATOOLS_API FramePainter : public Processor {
public:
    FramePainter();
    virtual ~FramePainter();
    

    virtual void process() override;

    //:COMMENT:28/02/2021:MONTFERME:Event handler functions
    void mousePaintEvent(Event* theevent);
    void mousePaintUnwantEvent(Event* theevent);
    void mouseEraseEvent(Event* theevent);
    void clearMaskEvent(Event* thevent);
    void undoPainting(Event* thevent);

    virtual const ProcessorInfo getProcessorInfo() const override;
    static const ProcessorInfo processorInfo_;

private:
    void paint(Event* theevent);
    void applyBrush(int mask,dvec2 pos,int radius);

    //:COMMENT:18/03/2021:MONTFERME:Information fetching methods
    int getWorkingMask(std::vector<Processor*> processorList);
    const dvec3 getCurrentSlices();
    dvec2 getPos2D(int mask,dvec2 mousePos,uvec2 canvasSize);
    dvec3 getPos3D(int mask, dvec2 pos2D);
    void get2DMaskValue();   
    dvec4 getColorFromMask3D(double state);

    void onThresholdChange();

    //:COMMENT:18/03/2021:MONTFERME:Initialization and update methods for masks
    void init2DMask(int mask);
    void init3DMask();
    void update2DMask(int mask,size3_t pos);
    void update3DMask(size3_t pos);
    void clearMasks();
    void checkAndResetMasks();

    //:COMMENT:18/03/2021:MONTFERME:Undo state storage method
    void saveState();

    //:COMMENT:04/03/2021:MONTFERME:Ports attributes
    VolumeInport volumeInport_;
    ImageInport inportX_,inportY_,inportZ_;
    ImageOutport maskOutportX_,maskOutportY_,maskOutportZ_;
    VolumeOutport maskOutport3D_;

    //:COMMENT:01/03/2021:MONTFERME:Event attributes
    CompositeProperty mouseEventsProperties_;
    EventProperty mousePaint_;
    EventProperty mousePaintUnwant_;
    EventProperty mouseErase_;
    EventProperty resetMask_;
    EventProperty undoPaint_;

    //:COMMENT:11/03/2021:MARTIN-DELOZANNE:Sliders attributes
    CompositeProperty thresholdingProperties_;
    FloatProperty max_;
    FloatProperty min_;

    //:COMMENT:01/03/2021:MONTFERME:Data attributes
    std::shared_ptr<Image> masks_[3];
    //:COMMENT:02/03/2021:MONTFERME:3D mask create from painting,
    std::shared_ptr<Volume> mask3D_;

    CompositeProperty slicesProperties_;
    std::vector<IntSizeTProperty> sliceNumbers_;

    //:COMMENT:01/03/2021:GUESDON:Control attributes
    CompositeProperty brushProperties_;
    IntSizeTProperty brush_;
    PaintState paintState_;

    //:COMMENT:15/03/2021:GUESDON:Undo functionnality attributes
    std::vector<std::tuple<std::shared_ptr<Volume>,std::shared_ptr<Image>,std::shared_ptr<Image>,std::shared_ptr<Image>>> memory_;
    int memoryLimit_ = 20;

};

}  // namespace inviwo
